# Distance rule computation

This is a proof of concept of efficient distance rule computation using powers of adjacency matrix.

Compared to the recursive graph exploration algorithm which is exponential time and linear space, it's cubic time and quadratic space.

## Algorithm

Let the [adjacency matrix](https://en.wikipedia.org/wiki/Adjacency_matrix) _A_, of coordinates _A(i,j)_ equal to _1_ if _j_ has certified _i_, _0_ else. We use the fact that _(A^p)(i,j)_ is the number of paths of length _p_ from _j_ to _i_.

Hence, _A+A^2_ contains the numbers of paths of length _1_ or _2_; _A+A^2+A^3_ the paths of length _1_, _2_ or _3_; and so on.

If _M(p+1) = M(p)+A·M(p)_ then _M(p)_ contains the numbers of paths of length up to _p_. We also have _M(p+1) = (A+1)·M(p)_ which is faster to compute because _A+1_ is constant.

Since we want to know whether a path exists between two members but do not care about the number of paths, we can replace arithmetical operations by boolean operations, and then store each coefficient as a single byte. Binary matrix multiplication is also faster due to lazy evaluation. (note that we are not using addition `mod 2`, but saturating addition, aka logical OR)

If the members are sorted "referent first" and there are _R_ referents, to get the number of accessible referents for the member _i_, we just need to count the number of _1_ in the _R_ first columns of the _i_th line.

Needed memory is a bit more than _3×N^2_ bytes.

### Optimizations

* Multithreaded binary matrix product
* Lazily-evaluated sum in binary matrix product

## To do

* Fix edge-case when the recursive algo finds a slightly different result for small matrices.
* Remove unsafe
* Hunt useless bound checks
* Improve cache locality in matrix product using transposition? Theoretically useful, but no significant improvement was detected when this was tested. Cache locality could also be improved using submatrices.
* SIMD?
* Store 8 coefficients by byte?
* Distributed computation?
* Filesystem memory-mapping?
* Try to store only 2 matrices instead of 3 (in-place matrix product)
* Optimize matrix building and path counting
* Nicer API
* Better testing with better data

## License

GNU AGPL v3, CopyLeft 2022 Pascal Engélibert

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3 of the License.  
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.  
You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/.
