use rayon::prelude::*;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct BinMatrix {
	pub dim: usize,
	pub coefs: Vec<bool>,
}

impl BinMatrix {
	pub fn zeros(dim: usize) -> Self {
		Self {
			dim,
			coefs: vec![false; dim * dim],
		}
	}

	pub fn or_identity(&mut self) {
		self.coefs
			.iter_mut()
			.step_by(self.dim + 1)
			.for_each(|c| *c = true)
	}

	pub fn or(&mut self, b: &Self) {
		assert_eq!(self.coefs.len(), b.coefs.len());
		self.coefs
			.iter_mut()
			.zip(b.coefs.iter())
			.for_each(|(a, b)| *a |= b);
	}

	pub fn dot(a: &Self, b: &Self, r: &mut Self) {
		let n = r.dim;
		assert_eq!(r.coefs.len(), n * n);
		assert_eq!(r.coefs.len(), a.coefs.len());
		assert_eq!(r.coefs.len(), b.coefs.len());

		r.coefs
			.par_chunks_exact_mut(n)
			.enumerate()
			.for_each(|(i, ri)| {
				'j: for (j, rij) in ri.iter_mut().enumerate() {
					if unsafe { *b.coefs.get_unchecked(i * n + j) } {
						*rij = true;
						continue 'j;
					}
					for k in 0..n {
						if unsafe {
							*a.coefs.get_unchecked(i * n + k) && *b.coefs.get_unchecked(k * n + j)
						} {
							*rij = true;
							continue 'j;
						}
					}
					*rij = false;
				}
			});
	}

	pub fn dot_tr(a: &Self, b: &Self, r: &mut Self) {
		let n = r.dim;
		assert_eq!(r.coefs.len(), n * n);
		assert_eq!(r.coefs.len(), a.coefs.len());
		assert_eq!(r.coefs.len(), b.coefs.len());

		r.coefs
			.par_chunks_exact_mut(n)
			.zip(a.coefs.par_chunks_exact(n))
			.for_each(|(ri, ai)| {
				'j: for (rij, bj) in ri.iter_mut().zip(b.coefs.chunks_exact(n)) {
					for (aik, bjk) in ai.iter().zip(bj.iter()) {
						if *aik && *bjk {
							*rij = true;
							continue 'j;
						}
					}
					*rij = false;
				}
			});
	}

	pub fn transpose(&mut self) {
		let n = self.dim;
		assert_eq!(n * n, self.coefs.len());
		for i in 0..n - 1 {
			let (top, bottom) = self.coefs.split_at_mut((i + 1) * n);
			for j in i + 1..n {
				unsafe {
					let s = *top.get_unchecked(i * n + j);
					*top.get_unchecked_mut(i * n + j) = *bottom.get_unchecked((j - i - 1) * n + i);
					*bottom.get_unchecked_mut((j - i - 1) * n + i) = s;
				}
			}
		}
	}

	pub fn _print(&self) {
		for row in self.coefs.chunks_exact(self.dim) {
			println!(
				"{}",
				row.iter()
					.map(|c| match c {
						true => '1',
						false => '·',
					})
					.collect::<String>()
			);
		}
	}
}
