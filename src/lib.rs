mod matrix;

pub use matrix::BinMatrix;

use rand::distributions::Distribution;

type AccountId = u32;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Account {
	pub issued: Vec<AccountId>,
	pub received: Vec<AccountId>,
}

impl Account {
	fn is_referent(&self, certs_min: u32) -> bool {
		self.issued.len() as u32 >= certs_min && self.received.len() as u32 >= certs_min
	}
}

fn sort_referents(accounts: &mut [Account], certs_min: u32) -> (u32, Vec<AccountId>) {
	let mut index: Vec<AccountId> = (0u32..accounts.len() as u32).collect();
	let mut i = 0;
	let mut j = accounts.len() - 1;
	while i < j {
		while accounts[i].is_referent(certs_min) && i < j {
			i += 1;
		}
		while !accounts[j].is_referent(certs_min) && i < j {
			j -= 1;
		}
		if i < j {
			accounts.swap(i, j);
			index.swap(i, j);
		}
	}

	(i as u32, index)
}

fn build_matrix(accounts: &[Account], index: &[AccountId]) -> BinMatrix {
	let mut matrix = BinMatrix::zeros(accounts.len());

	for (i, account) in accounts.iter().enumerate() {
		for &j in account.received.iter() {
			matrix.coefs[index[j as usize] as usize + accounts.len() * i as usize] = true;
		}
	}

	matrix
}

pub fn distance_rule_matrix(mut accounts: Vec<Account>, certs_min: u32, dist: u32) -> Vec<u32> {
	let (nb_referents, index) = sort_referents(&mut accounts, certs_min);
	//println!("Nb referents: {}", nb_referents);

	let mut paths = build_matrix(&accounts, &index);
	let mut res = BinMatrix::zeros(accounts.len());
	let mut matrix = paths.clone();
	matrix.or_identity();

	//paths.print();
	//println!("built");

	for _i in 0..dist {
		BinMatrix::dot(&matrix, &paths, &mut res);
		std::mem::swap(&mut paths, &mut res);
		//paths.print();
		//println!("{}", i);
	}

	// `paths` is then the adjacency matrix for paths of length <= dist

	index
		.into_iter()
		.map(|i| i as usize)
		.map(|i| {
			paths.coefs[i * paths.dim..i * paths.dim + nb_referents as usize]
				.iter()
				.filter(|&n| *n)
				.count() as u32
		})
		.collect()
}

pub fn distance_rule_matrix2(mut accounts: Vec<Account>, certs_min: u32, dist: u32) -> Vec<u32> {
	let (nb_referents, index) = sort_referents(&mut accounts, certs_min);
	//println!("Nb referents: {}", nb_referents);

	let mut paths = build_matrix(&accounts, &index);
	let mut res = BinMatrix::zeros(accounts.len());
	let matrix = paths.clone();
	let mut identity = BinMatrix::zeros(accounts.len());
	identity.or_identity();

	//paths.print();
	//println!("built");

	for _i in 0..dist {
		BinMatrix::dot(&matrix, &paths, &mut res);
		paths.or(&res);
		//paths.print();
		//println!("{}", i);
	}

	// `paths` is then the adjacency matrix for paths of length <= dist

	index
		.into_iter()
		.map(|i| i as usize)
		.map(|i| {
			paths.coefs[i * paths.dim..i * paths.dim + nb_referents as usize]
				.iter()
				.filter(|&n| *n)
				.count() as u32
		})
		.collect()
}

pub fn distance_rule_matrix_tr(mut accounts: Vec<Account>, certs_min: u32, dist: u32) -> Vec<u32> {
	let (nb_referents, index) = sort_referents(&mut accounts, certs_min);
	//println!("Nb referents: {}", nb_referents);

	let mut paths = build_matrix(&accounts, &index);
	let mut res = BinMatrix::zeros(accounts.len());
	let mut matrix = paths.clone();
	matrix.or_identity();
	paths.transpose();

	//paths.print();
	//println!("built");

	for _i in 0..dist {
		BinMatrix::dot_tr(&paths, &matrix, &mut res);
		std::mem::swap(&mut paths, &mut res);
		//paths.print();
		//println!("{}", i);
	}
	paths.transpose();

	// `paths` is then the adjacency matrix for paths of length <= dist

	index
		.into_iter()
		.map(|i| i as usize)
		.map(|i| {
			paths.coefs[i * paths.dim..i * paths.dim + nb_referents as usize]
				.iter()
				.filter(|&n| *n)
				.count() as u32
		})
		.collect()
}

fn distance_rule_tree_recursive(
	accounts: &[Account],
	account_id: u32,
	accessible: &mut std::collections::HashSet<u32>,
	depth: u32,
	certs_min: u32,
) {
	let account = &accounts[account_id as usize];
	if account.is_referent(certs_min) {
		accessible.insert(account_id);
	}
	if depth == 0 {
		return;
	}
	for &received in account.received.iter() {
		distance_rule_tree_recursive(accounts, received, accessible, depth - 1, certs_min);
	}
}

pub fn distance_rule_tree(accounts: &[Account], certs_min: u32, dist: u32) -> Vec<u32> {
	let mut accessible = std::collections::HashSet::<u32>::new();
	(0..accounts.len() as u32)
		.map(|account_id| {
			accessible.clear();
			distance_rule_tree_recursive(
				accounts,
				account_id,
				&mut accessible,
				dist + 1,
				certs_min,
			);
			accessible.len() as u32
		})
		.collect()
}

fn _main() {
	let nb_accounts = 5_000;
	let certs_min = 5;
	let dist = 5;

	let mut rng1 = rand::thread_rng();
	let mut rng2 = rand::thread_rng();
	let distrib_account_id = rand::distributions::Uniform::<u32>::new(0, nb_accounts);
	let distrib_cert = rand::distributions::Bernoulli::new(0.895).unwrap();

	let mut nb_certs = 0usize;
	let mut accounts: Vec<Account> = (0..nb_accounts)
		.map(|i| Account {
			received: (0..nb_accounts - 1)
				.take_while(|_| distrib_cert.sample(&mut rng1))
				.map(|_| {
					nb_certs += 1;
					loop {
						let issuer = distrib_account_id.sample(&mut rng2);
						if issuer != i {
							return issuer;
						}
					}
				})
				.collect(),
			issued: vec![],
		})
		.collect();
	for i in 0..accounts.len() {
		for issuer in accounts[i].received.clone() {
			accounts[issuer as usize].issued.push(i as u32);
		}
	}
	println!(
		"Certs: {} (mean {} certs/member)",
		nb_certs,
		nb_certs as f32 / nb_accounts as f32
	);

	distance_rule_matrix(accounts, certs_min, dist);
}

#[cfg(test)]
mod test {
	use super::*;

	fn gen_ref() -> Account {
		Account {
			issued: vec![1, 2, 3, 4, 5],
			received: vec![1, 2, 3, 4, 5],
		}
	}

	fn gen_noref() -> Account {
		Account {
			issued: vec![],
			received: vec![],
		}
	}

	#[test]
	fn test_sort_referents() {
		let accounts_bak = vec![
			gen_ref(),
			gen_noref(),
			gen_ref(),
			gen_ref(),
			gen_noref(),
			gen_ref(),
		];
		let mut accounts = accounts_bak.clone();
		let (nb_referents, index) = sort_referents(&mut accounts, 5);
		assert_eq!(
			&accounts,
			&[
				gen_ref(),
				gen_ref(),
				gen_ref(),
				gen_ref(),
				gen_noref(),
				gen_noref()
			]
		);
		assert_eq!(nb_referents, 4);
		for (a, b) in accounts_bak
			.iter()
			.enumerate()
			.map(|(i, a)| (a, &accounts[index[i] as usize]))
		{
			assert_eq!(a, b);
		}
	}

	#[test]
	fn test_sort_referents_random() {
		let nb_accounts = 100;

		let mut rng = rand::thread_rng();
		let distrib_cert = rand::distributions::Bernoulli::new(0.5).unwrap();
		let mut nb_refs = 0u32;
		let accounts_bak: Vec<Account> = (0..nb_accounts)
			.map(|_| {
				if distrib_cert.sample(&mut rng) {
					nb_refs += 1;
					gen_ref()
				} else {
					gen_noref()
				}
			})
			.collect();
		let mut accounts = accounts_bak.clone();
		let (s_nb_refs, s_index) = sort_referents(&mut accounts, 5);
		assert_eq!(s_nb_refs, nb_refs);
		(0..nb_refs)
			.zip(accounts.iter())
			.for_each(|(_, account)| assert!(account.is_referent(5)));
		accounts
			.iter()
			.skip(nb_refs as usize)
			.for_each(|account| assert!(!account.is_referent(5)));
		s_index.iter().for_each(|&i| assert!(i < nb_accounts));
	}

	#[test]
	fn test_distance_rule() {
		let nb_accounts = 200;
		let certs_min = 5;
		let dist = 5;

		let mut rng1 = rand::thread_rng();
		let mut rng2 = rand::thread_rng();
		let distrib_account_id = rand::distributions::Uniform::<u32>::new(0, nb_accounts);
		let distrib_cert = rand::distributions::Bernoulli::new(0.85).unwrap();

		let mut nb_certs = 0usize;
		let mut accounts: Vec<Account> = (0..nb_accounts as u32)
			.map(|i| Account {
				received: (0..nb_accounts - 1)
					.take_while(|_| distrib_cert.sample(&mut rng1))
					.map(|_| {
						nb_certs += 1;
						loop {
							let issuer = distrib_account_id.sample(&mut rng2);
							if issuer != i {
								return issuer;
							}
						}
					})
					.collect(),
				issued: vec![],
			})
			.collect();
		for i in 0..accounts.len() {
			for issuer in accounts[i].received.clone() {
				accounts[issuer as usize].issued.push(i as u32);
			}
		}
		println!(
			"Certs: {} (mean {} certs/member)",
			nb_certs,
			nb_certs as f32 / nb_accounts as f32
		);

		println!("Computing tree...");
		let result_tree = distance_rule_tree(&accounts, certs_min, dist);

		println!("Computing matrix...");
		let result_matrix = distance_rule_matrix(accounts.clone(), certs_min, dist);
		println!("{:?}", result_matrix);
		assert_eq!(result_matrix, result_tree);

		println!("Computing matrix2...");
		let result_matrix2 = distance_rule_matrix2(accounts.clone(), certs_min, dist);
		assert_eq!(result_matrix, result_matrix2);

		println!("Computing matrix_tr...");
		let result_matrix_tr = distance_rule_matrix_tr(accounts, certs_min, dist);
		assert_eq!(result_matrix, result_matrix_tr);
	}
}
