use distrule::*;

use criterion::{black_box, criterion_group, criterion_main, BenchmarkId, Criterion, Throughput};
use rand::distributions::Distribution;
use std::time::{Duration, Instant};

pub fn distance_rule(c: &mut Criterion) {
	let mut group = c.benchmark_group("distance_rule");
	for nb_accounts in [10, 20, 50, 100, 200] {
		let certs_min = 5;
		let dist = 5;

		let mut rng1 = rand::thread_rng();
		let mut rng2 = rand::thread_rng();
		let distrib_account_id = rand::distributions::Uniform::<u32>::new(0, nb_accounts);
		let distrib_cert = rand::distributions::Bernoulli::new(0.895).unwrap();

		let mut nb_certs = 0usize;
		let mut accounts: Vec<Account> = (0..nb_accounts)
			.map(|i| Account {
				received: (0..nb_accounts - 1)
					.take_while(|_| distrib_cert.sample(&mut rng1))
					.map(|_| {
						nb_certs += 1;
						loop {
							let issuer = distrib_account_id.sample(&mut rng2);
							if issuer != i {
								return issuer;
							}
						}
					})
					.collect(),
				issued: vec![],
			})
			.collect();
		for i in 0..accounts.len() {
			for issuer in accounts[i].received.clone() {
				accounts[issuer as usize].issued.push(i as u32);
			}
		}

		group.throughput(Throughput::Elements(nb_accounts as u64));
		group.bench_with_input(
			BenchmarkId::new("matrix", nb_accounts),
			&nb_accounts,
			|b, _nb_accounts| {
				b.iter_custom(|iters| {
					let mut total = Duration::new(0, 0);
					for _i in 0..iters {
						let accounts = accounts.clone();
						let start = Instant::now();
						black_box(distance_rule_matrix(accounts, certs_min, dist));
						total += start.elapsed();
					}
					total
				})
			},
		);
		group.bench_with_input(
			BenchmarkId::new("matrix2", nb_accounts),
			&nb_accounts,
			|b, _nb_accounts| {
				b.iter_custom(|iters| {
					let mut total = Duration::new(0, 0);
					for _i in 0..iters {
						let accounts = accounts.clone();
						let start = Instant::now();
						black_box(distance_rule_matrix2(accounts, certs_min, dist));
						total += start.elapsed();
					}
					total
				})
			},
		);
		group.bench_with_input(
			BenchmarkId::new("matrix_tr", nb_accounts),
			&nb_accounts,
			|b, _nb_accounts| {
				b.iter_custom(|iters| {
					let mut total = Duration::new(0, 0);
					for _i in 0..iters {
						let accounts = accounts.clone();
						let start = Instant::now();
						black_box(distance_rule_matrix_tr(accounts, certs_min, dist));
						total += start.elapsed();
					}
					total
				})
			},
		);
		/*group.bench_with_input(BenchmarkId::new("tree", nb_accounts), &nb_accounts, |b, _nb_accounts| {
			b.iter(|| {
				distance_rule_tree(&accounts, certs_min, dist)
			})
		});*/
	}
	group.finish();
}

criterion_group!(benches, distance_rule);
criterion_main!(benches);
